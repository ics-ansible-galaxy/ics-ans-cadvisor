import os
import testinfra.utils.ansible_runner
import json


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_container(host):
    with host.sudo():
        cadvisor = host.docker("cadvisor")
        assert cadvisor.is_running


def test_listening(host):
    assert host.socket("tcp://9191").is_listening


def test_metrics(host):
    cmd = host.run("curl --insecure --fail http://ics-ans-cadvisor-default:9191/metrics")
    assert cmd.rc == 0
    assert "cadvisor_version_info" in cmd.stdout


def test_api(host):
    cmd = host.run("curl --insecure --fail http://ics-ans-cadvisor-default:9191/api/v1.3/containers/docker")
    assert cmd.rc == 0
    assert len(json.loads(cmd.stdout)['subcontainers']) == 1
